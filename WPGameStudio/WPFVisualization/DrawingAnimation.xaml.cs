﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Threading;

namespace WPFVisualization
{
    /// <summary>
    /// Interaction logic for DrawingAnimation.xaml
    /// </summary>
    public partial class DrawingAnimation : Canvas
    {
        int _lastIdx = 0;
        Dictionary<int, Tuple<BitmapImage, Size>> SpritCollection = new Dictionary<int, Tuple<BitmapImage, Size>>();

        private bool _animationEnabled;
        public bool Enabled { get { return _animationEnabled; } }

        private int _interval;
        public int Interval { get { return _interval; } }

        public void LoadSpritCollection(int min, int max, string pattern)
        {
            if (_resourceHandler == null)
                return;

            if (_assemblyName == null)
                _assemblyName = _resourceHandler.SourceAssembly.FullName.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries)[0];
            List<Uri> sourceList = _spritUriFactory.CreateSpritUriListUntillNoResourceFound(min, max, pattern, "pack://application:,,,/" + _assemblyName + ";component/");
            SpritCollection.Clear();
            int idx = 0;
            foreach (var uri in sourceList)
            {
                try
                {
                    BitmapImage image = new BitmapImage();
                    if (uri != null)
                    {
                        image.BeginInit();
                        image.UriSource = uri;
                        image.CacheOption = BitmapCacheOption.OnLoad;
                        image.EndInit();
                    }
                    SpritCollection.Add(idx, new Tuple<BitmapImage, Size>(image, new Size(image.PixelWidth, image.PixelHeight)));
                    idx++;
                }
                catch (Exception ex)
                {
                    break;
                }
            }
        }

        DispatcherTimer _nextFrameTimer = new DispatcherTimer();

        private string _assemblyName;
        private ResourceHandler _resourceHandler;
        private SpritUriFactory _spritUriFactory;

        private double? _fixedWidth;
        private double? _fixedHeight;
        private double _fallingTime = 0;
        private double _yAxeDistance = 0;
        private double _xAxeDistance = 0;
        private double _startPosY;
        private double _startPosX;
        private double _slippingTime = 0;
        private DrawingAnimationViewModel _viewModel = new DrawingAnimationViewModel();

        public void SetFallingTime(int interval)
        {
            _fallingTime = interval;
        }

        public void Stop()
        {
            _nextFrameTimer.Stop();
            _animationEnabled = false;
            _lastIdx = 0;
            drawingArea.Source = null;
            StopRotation();
        }

        public void Pause()
        {
            _nextFrameTimer.Stop();
            StopRotation();
            _animationEnabled = false;
        }

        public void Start(int? interval = null)
        {
            SetFrameInterval(interval);
            SetNextFrameTimer();
            _animationEnabled = true;
        }

        private void SetFrameInterval(int? interval)
        {
            if (interval != null)
                _interval = (int) interval;
            else
                _interval = 80;
        }

        private void SetNextFrameTimer()
        {
            _nextFrameTimer.Interval = TimeSpan.FromMilliseconds(Interval);
            _nextFrameTimer.Start();
        }

        public DrawingAnimation()
        {
            InitializeComponent();
            InitializeDefaultValues();
        }

        private void InitializeDefaultValues()
        {
            _nextFrameTimer.Tick += new EventHandler(async (object s, EventArgs a) =>
            {
                await Task.Run(() => NextFrameTimerTick(s, a));
            });
        }

        public bool Repeat { get; set; }

        public ResourceHandler ResourceHandler
        {
            set { SetAnimationSource(value); }
            get { return _resourceHandler; }
        }

        private void SetAnimationSource(ResourceHandler value)
        {
            _resourceHandler = value;
            _spritUriFactory = new SpritUriFactory(_resourceHandler);
        }

        public double? FixedWidth
        {
            get { return _fixedWidth; }
            set { _fixedWidth = value; }
        }

        public double? FixedHeight
        {
            get { return _fixedWidth; }
            set { _fixedWidth = value; }
        }

        public DrawingAnimationViewModel ViewModel
        {
            get { return _viewModel; }
            set { _viewModel = value; }
        }

        void NextFrameTimerTick(object sender, EventArgs e)
        {
            Tuple<BitmapImage, Size> image;
            SpritCollection.TryGetValue(_lastIdx, out image);
            if (image != null)
            {
                ViewModel.Source = image.Item1;
                ViewModel.Height = image.Item2.Height;
                ViewModel.Width = image.Item2.Width;
            }
            _lastIdx++;
            if (_lastIdx >= SpritCollection.Count)
            {
                if (Repeat)
                    _lastIdx = 0;
                else
                    Stop();
            }
        }

        public void StartRotation(int interval)
        {
            Storyboard s = (Storyboard)TryFindResource("storyBoard");
            s.SpeedRatio = interval;
            s.Begin();
        }

        public void StopRotation()
        {
            Storyboard s = (Storyboard)TryFindResource("storyBoard");
            s.Stop();
        }

        public void StartMoveByYAxe()
        {
            DoubleAnimation animation;
            var s = SetMoveStoryboard(new PropertyPath(Canvas.TopProperty), out animation, "StoryBoardFalling");
            animation.To = _startPosY + _yAxeDistance;
            animation.From = _startPosY;
            s.Children.Add(animation);
            s.Begin();
        }

        public void StartMoveByXAxe()
        {
            DoubleAnimation animation;
            var s = SetMoveStoryboard(new PropertyPath(Canvas.LeftProperty), out animation, "StoryBoardSlipping");
            animation.To = _startPosX + _xAxeDistance;
            animation.From = _startPosX;
            s.Children.Add(animation);
            s.Begin();
        }

        private Storyboard SetMoveStoryboard(PropertyPath propertrtyPath, out DoubleAnimation animation, string resourceKey)
        {
            Storyboard s = (Storyboard) TryFindResource(resourceKey);
            animation = new DoubleAnimation();
            animation.Duration = new Duration(TimeSpan.FromMilliseconds(_fallingTime));
            Storyboard.SetTarget(animation, this);
            Storyboard.SetTargetProperty(animation, propertrtyPath);
            return s;
        }

        public void StopFalling()
        {
            Storyboard s = (Storyboard)TryFindResource("storyBoard2");
            s.Stop();
        }

        private void DoubleAnimation_Completed_1(object sender, EventArgs e)
        {
            Storyboard s = (Storyboard)TryFindResource("storyBoard");
            s.Begin();
        }

        public void SetStartPosition(double fallStartPosX, double fallStartPosY)
        {
            _startPosX = fallStartPosX;
            _startPosY = fallStartPosY;
        }

        public void SetSlippingTime(int interval)
        {
            _slippingTime = interval;
        }

        public void SetFallingDistance(double fallingDistance)
        {
            _yAxeDistance = fallingDistance;
        }

        public void SetSlippingDistance(double slippingDistance)
        {
            _xAxeDistance = slippingDistance;
        }
    }
}
