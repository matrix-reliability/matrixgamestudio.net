using System;
using System.Collections.Generic;

namespace WPFVisualization
{
    public class SpritUriFactory
    {
        private ResourceHandler _resourceHandler;

        public SpritUriFactory(ResourceHandler resourceHandler)
        {
            _resourceHandler = resourceHandler;
        }

        public List<Uri> CreateSpritUriListUntillNoResourceFound(int min, int max, string pattern, string appResource)
        {
            List<Uri> list = new List<Uri>();
            for (int idx = min; idx <= max; idx++)
            {
                string path = appResource;
                string patt = pattern.Replace("*", idx + "");
                var uriString = path + patt;
                if (_resourceHandler.ResourceExists(patt.ToLower()))
                {
                    list.Add(new Uri(uriString, UriKind.Absolute));
                }
                else
                    break;
            }
            return list;
        }
    }
}