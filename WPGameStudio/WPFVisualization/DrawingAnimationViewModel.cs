using System.ComponentModel;
using System.Windows;
using System.Windows.Media;

namespace WPFVisualization
{
    public class DrawingAnimationViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private ImageSource _source;
        private double _width, _height;

        public ImageSource Source
        {
            get { return _source; }
            set
            {
                if (Equals(value, _source)) 
                    return;
                _source = value;
                OnPropertyChanged("Source");
            }
        }

        public double Width
        {
            get { return _width; }
            set
            {
                _width = value;
                OnPropertyChanged("Width");
            }
        }

        public double Height
        {
            get { return _height; }
            set
            {
                _height = value;
                OnPropertyChanged("Height");
            }
        }

        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}