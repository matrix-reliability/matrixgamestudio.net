using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Resources;

namespace WPFVisualization
{
    public class ResourceHandler
    {
        private Assembly _sourceAssembly;
        private IEnumerable<string> _resourcePaths;

        public Assembly SourceAssembly 
        {
            get
            {
                return
                    _sourceAssembly;
            }
        }

        public ResourceHandler(Assembly sourceAssembly)
        {
            _sourceAssembly = sourceAssembly;
            _resourcePaths = GetResourcePaths(sourceAssembly);
        }
        
        public bool ResourceExists(string resourcePath)
        {
            var list = _resourcePaths.ToList();
            return list.Any(x => x == resourcePath);
        }

        public IEnumerable<string> GetResourcePaths(Assembly assembly)
        {
            var culture = System.Threading.Thread.CurrentThread.CurrentCulture;
            var resourceName = assembly.GetName().Name + ".g";
            var resourceManager = new ResourceManager(resourceName, assembly);

            try
            {
                var resourceSet = resourceManager.GetResourceSet(culture, true, true);

                foreach(System.Collections.DictionaryEntry resource in resourceSet)
                {
                    yield return (string)resource.Key;
                }
            }
            finally
            {
                resourceManager.ReleaseAllResources();
            }
        }
    }
}