using System;
using System.Reflection;
using WPFVisualization;

namespace WPFTester
{
    public class AnimationFactory
    {
        private readonly ResourceHandler _resourceHandler;
        private string _executingAssembly;

        public AnimationFactory(ResourceHandler resourceHandler)
        {
            _resourceHandler = resourceHandler;
        }

        public DrawingAnimation CreateOneLifeAnimation(int rotation, string pattern)
        {
            var visual = new DrawingAnimation();
            visual.StartRotation(rotation);
            visual.ResourceHandler = _resourceHandler;
            visual.LoadSpritCollection(1, Int32.MaxValue, pattern);
            return visual;
        }

        public DrawingAnimation CreateRepeatableAnimation(int rotation, string pattern)
        {
            var visual = new DrawingAnimation();
            visual.StartRotation(rotation);
            visual.Repeat = true;
            visual.ResourceHandler = _resourceHandler;
            visual.LoadSpritCollection(1, Int32.MaxValue, pattern);
            return visual;
        }

        public DrawingAnimation CreateRepeatableFallingAnimation(
            int rotation, 
            string pattern, 
            MoveConfig fallingConfig,
            MoveConfig slippingConfig)
        {
            var visual = new DrawingAnimation();
            visual.StartRotation(rotation);
            visual.Repeat = true;
            visual.ResourceHandler = _resourceHandler;
            visual.LoadSpritCollection(1, Int32.MaxValue, pattern);
            visual.SetFallingTime(fallingConfig.Time);
            visual.SetFallingDistance(fallingConfig.Distance);
            visual.SetSlippingTime(slippingConfig.Time);
            visual.SetSlippingDistance(slippingConfig.Distance);
            visual.SetStartPosition(slippingConfig.StartAt, fallingConfig.StartAt);
            visual.StartMoveByXAxe();
            visual.StartMoveByYAxe();
            return visual;
        }
    }
}