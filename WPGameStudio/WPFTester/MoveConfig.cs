namespace WPFTester
{
    public struct MoveConfig
    {
        public MoveConfig(int time, double distance, double startPoint) : this()
        {
            Time = time;
            Distance = distance;
            StartAt = startPoint;
        }

        public int Time { get; set; }
        public double Distance { get; set; }
        public double StartAt { get; set; }
    }
}