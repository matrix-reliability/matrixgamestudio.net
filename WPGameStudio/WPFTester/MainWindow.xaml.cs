﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Windows.Threading;
using Microsoft.Win32;
using WPFVisualization;

namespace WPFTester
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private ResizeMode _remeberPrevResizeMode;
        private WindowStyle _remeberPrevWindowStyle;
        private WindowState _remeberPrevWindowState;
        private AnimationFactory _animationFactory;
        private List<DrawingAnimation> _animationList;

        public MainWindow()
        {
            _animationList = new List<DrawingAnimation>();
            InitializeComponent();
            InitCompositionRoot();
        }

        private void InitCompositionRoot()
        {
            var resHandler = new ResourceHandler(Assembly.GetExecutingAssembly());
            _animationFactory = new AnimationFactory(resHandler);
            this.KeyDown += MainWindow_KeyDown;
            this.KeyUp += MainWindow_KeyUp; 
            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds(1);
            timer.Tick += timer_Tick;
            timer.Start();
        }

        void timer_Tick(object sender, EventArgs e)
        {
            if ((mePlayer.Source != null) && (mePlayer.NaturalDuration.HasTimeSpan) && (!_userIsDraggingSlider))
            {
                sliProgress.Minimum = 0;
                sliProgress.Maximum = mePlayer.NaturalDuration.TimeSpan.TotalSeconds;
                sliProgress.Value = mePlayer.Position.TotalSeconds;
            }
            if (mePlayer.Source != null)
            {
                if (mePlayer.NaturalDuration.HasTimeSpan)
                    lblStatus.Content = String.Format("{0} / {1}", mePlayer.Position.ToString(@"mm\:ss"), mePlayer.NaturalDuration.TimeSpan.ToString(@"mm\:ss"));
            }
            else
                lblStatus.Content = "No file selected...";
        }

        private void btnPlay_Click(object sender, RoutedEventArgs e)
        {
            mePlayer.Width = Double.Parse(playerWidth.Text);
            mePlayer.Height = Double.Parse(playerHeight.Text);
            mePlayer.Play();
            _isPlayed = true;
        }

        private void btnPause_Click(object sender, RoutedEventArgs e)
        {
            mePlayer.Pause();
            _isPlayed = false;
        }

        private void btnStop_Click(object sender, RoutedEventArgs e)
        {
            mePlayer.Stop();
            _isPlayed = false;
        }

        bool _isPlayed = false;
        void MainWindow_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Left)
            {
                mePlayer.Position = mePlayer.Position.Add(-TimeSpan.FromSeconds(3)); ;
            }
            if (e.Key == Key.Right)
            {
                mePlayer.Position = mePlayer.Position.Add(TimeSpan.FromSeconds(3)); ;
            }
            if (e.Key == Key.Space)
            {
                if (_isPlayed)
                    btnPause_Click(sender, e);
                else
                    btnPlay_Click(this, e);
            }
        }

        void MainWindow_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                SetFullscreenOn();
            }
            if (e.Key == Key.Escape)
            {
                SetFulscrenOff();
                btnPause_Click(sender, e);
            }
            if (e.Key == Key.F5)
            {
                if (!_isPlayed)
                    btnPlay_Click(this, e);
                else
                    btnStop_Click(this, e);
            }
        }

        public AnimationFactory AnimationFactory
        {
            get { return _animationFactory; }
        }

        private void InitializeAnimation(object sender, RoutedEventArgs e)
        {
            try
            {
                string pattern = patternTb.Text;
                int fallingDistance = 1024 + 680 * 2 + 100;
                int slippingDistance = 1600;
                int idx = 0;
                foreach (
                    string currentPatterm in pattern.Split(new char[] {';', ','}, StringSplitOptions.RemoveEmptyEntries)
                    )
                {
                    int randomLeft = new Random().Next(0, 1280);

                    var anim = AnimationFactory.CreateRepeatableFallingAnimation(
                        15,
                        currentPatterm,
                        new MoveConfig(5*1000, fallingDistance, -680),
                        new MoveConfig(1000, slippingDistance, _pos.X - 387/2 - (idx == 0 ? 0 : (double)randomLeft))
                        );
                    AnimationOwner.Children.Add(anim);
                    _animationList.Add(anim);
                    anim.Start();
                    idx++;
                }
            }
            catch (Exception ex)
            {
            }
        }

        private void RemoveAnimations(object sender, RoutedEventArgs e)
        {
            foreach (var drawingAnimation in _animationList)
            {
                AnimationOwner.Children.Remove(drawingAnimation);
            }
            _animationList.Clear();
            GC.Collect();
        }

        private DispatcherTimer _fullScreenVideoBegin;
        private DispatcherTimer _fullScreenVideoEnd;
        private Point _pos;
        private Rectangle _line;
        private Rectangle _line2;
        private bool _userIsDraggingSlider;

        private void MakeFullScreen(object sender, RoutedEventArgs e)
        {
            _fullScreenVideoBegin = new DispatcherTimer();
            _fullScreenVideoBegin.Tick += delegate
            {
                SetFullscreenOn();

                GuiForHide.Visibility = System.Windows.Visibility.Collapsed;
                _fullScreenVideoEnd = new DispatcherTimer();
                try
                {
                    _fullScreenVideoEnd.Interval = TimeSpan.FromMilliseconds(Int32.Parse(howManySecText.Text));
                }
                catch (Exception)
                {
                    _fullScreenVideoEnd.Interval = TimeSpan.FromMilliseconds(3 * 1000);
                }
                _fullScreenVideoEnd.Tick += delegate
                {
                    SetFulscrenOff();
                };
                _fullScreenVideoBegin.Stop();
                _fullScreenVideoEnd.Start();
            };
            _fullScreenVideoBegin.Interval = TimeSpan.FromMilliseconds(3 * 1000);
            _fullScreenVideoBegin.Start();
        }

        private void SetFullscreenOn()
        {
            RemoveAnimStartingPositionLine();
            this.Cursor = Cursors.None;
            _remeberPrevResizeMode = this.ResizeMode;
            _remeberPrevWindowStyle = this.WindowStyle;
            _remeberPrevWindowState = this.WindowState;

            GuiForHide.Visibility = Visibility.Collapsed;

            this.ResizeMode = ResizeMode.NoResize;
            this.WindowStyle = WindowStyle.None;
            this.WindowState = WindowState.Maximized;
        }

        private void RemoveAnimStartingPositionLine()
        {
            AnimationOwner.Children.Remove(_line);
            _line = null;
            AnimationOwner.Children.Remove(_line2);
            _line2 = null;
        }

        private void SetFulscrenOff()
        {
            this.ResizeMode = _remeberPrevResizeMode;
            this.WindowStyle = _remeberPrevWindowStyle;
            this.WindowState = _remeberPrevWindowState;
            this.Cursor = Cursors.Arrow;
            GuiForHide.Visibility = Visibility.Collapsed;
            GuiForHide.Visibility = Visibility.Visible;
            if (_fullScreenVideoEnd != null) _fullScreenVideoEnd.Stop();
        }

        private void MakeFullScreenNow(object sender, RoutedEventArgs e)
        {
            SetFullscreenOn();
        }

        private void OpenContextMenu(FrameworkElement element)
        {
            if (element.ContextMenu != null)
            {
                element.ContextMenu.PlacementTarget = element;
                element.ContextMenu.IsOpen = true;
            }
        }

        private void PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.RightButton == MouseButtonState.Pressed)
            {
                OpenContextMenu((FrameworkElement)sender);
            }
            if (e.LeftButton == MouseButtonState.Pressed)
                InitializeAnimation(sender, e);
        }

        private void AnimationOwner_OnMouseMove(object sender, MouseEventArgs e)
        {
            _pos = e.GetPosition(AnimationOwner);
            if (chboxShowCursor.IsChecked == true)
                Cursor = Cursors.Cross;
            else
                Cursor = Cursors.None;

            if (chboxSetPosition.IsChecked == true)
            {
                if (_line == null)
                {
                    _line = new Rectangle();
                    _line.Fill = new SolidColorBrush(Colors.Aquamarine);
                    _line.Width = 3;
                    AnimationOwner.Children.Add(_line);
                }
                if (_line2 == null)
                {
                    _line2 = new Rectangle();
                    _line2.Fill = new SolidColorBrush(Colors.Aquamarine);
                    _line2.Height = 3;
                    AnimationOwner.Children.Add(_line2);
                }
                _line.Height = this.Height + 1080;
                _line.SetValue(Canvas.LeftProperty, _pos.X);
                _line.SetValue(Canvas.TopProperty, -30.0);

                _line2.Width = this.Width + 1920;
                _line2.SetValue(Canvas.LeftProperty, -500.0);
                _line2.SetValue(Canvas.TopProperty, _pos.Y);
                return;
            }
            RemoveAnimStartingPositionLine();
        }

        private void btnLoad_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new OpenFileDialog();
            dialog.Title = "Choose Media";
            if (dialog.ShowDialog() == true)
            {
                mePlayer.Source = new Uri(dialog.FileName);
            }
        }

        private void sliProgress_DragStarted(object sender, DragStartedEventArgs e)
        {
            _userIsDraggingSlider = true;
        }

        private void sliProgress_DragCompleted(object sender, DragCompletedEventArgs e)
        {
            _userIsDraggingSlider = false;
            mePlayer.Position = TimeSpan.FromSeconds(sliProgress.Value);
        }

        private void sliProgress_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (!mePlayer.NaturalDuration.HasTimeSpan)
                return;
            lblStatus.Content = String.Format("{0} / {1}", mePlayer.Position.ToString(@"mm\:ss"), mePlayer.NaturalDuration.TimeSpan.ToString(@"mm\:ss"));
        }

    }
}
